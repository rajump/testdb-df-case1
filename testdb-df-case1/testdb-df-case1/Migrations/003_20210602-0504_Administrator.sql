﻿-- <Migration ID="cfd74e95-a65e-408d-b4fe-cd717121179e" />
GO


SET DATEFORMAT YMD;


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[EMP]) = 0
    BEGIN
        PRINT (N'Add 15 rows to [dbo].[EMP]');
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (1, 'JOHNSON', 'ADMIN', 6, '1990-12-17 00:00:00.000', 18000.00, NULL, 4, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (2, 'HARDING', 'MANAGER', 9, '1998-02-02 00:00:00.000', 52000.00, 300.00, 3, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (3, 'TAFT', 'SALES I', 2, '1996-01-02 00:00:00.000', 25000.00, 500.00, 3, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (4, 'HOOVER', 'SALES I', 2, '1990-04-02 00:00:00.000', 27000.00, NULL, 3, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (5, 'LINCOLN', 'TECH', 6, '1994-06-23 00:00:00.000', 22500.00, 1400.00, 4, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (6, 'GARFIELD', 'MANAGER', 9, '1993-05-01 00:00:00.000', 54000.00, NULL, 4, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (7, 'POLK', 'TECH', 6, '1997-09-22 00:00:00.000', 25000.00, NULL, 4, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (8, 'GRANT', 'ENGINEER', 10, '1997-03-30 00:00:00.000', 32000.00, NULL, 2, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (9, 'JACKSON', 'CEO', NULL, '1990-01-01 00:00:00.000', 75000.00, NULL, 4, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (10, 'FILLMORE', 'MANAGER', 9, '1994-08-09 00:00:00.000', 56000.00, NULL, 2, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (11, 'ADAMS', 'ENGINEER', 10, '1996-03-15 00:00:00.000', 34000.00, NULL, 2, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (12, 'WASHINGTON', 'ADMIN', 6, '1998-04-16 00:00:00.000', 18000.00, NULL, 4, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (13, 'MONROE', 'ENGINEER', 10, '2000-12-03 00:00:00.000', 30000.00, NULL, 2, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (14, 'ROOSEVELT', 'CPA', 9, '1995-10-12 00:00:00.000', 35000.00, NULL, 1, NULL);
        INSERT  INTO [dbo].[EMP] ([empno], [ename], [job], [mgr], [hiredate], [sal], [comm], [dept], [jobrole])
        VALUES                  (15, 'TONNY', 'RMP', 4, '1984-10-12 00:00:00.000', 38000.00, NULL, 1, NULL);
    END


GO