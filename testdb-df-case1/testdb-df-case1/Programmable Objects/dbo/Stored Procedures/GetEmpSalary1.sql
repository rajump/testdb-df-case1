IF OBJECT_ID('[dbo].[GetEmpSalary1]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetEmpSalary1];

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetEmpSalary1]( @empno int,@sal
numeric(7,2) output)AS SELECT @sal = sal from emp where empno = @empno;
GO
